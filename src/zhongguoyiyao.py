import re
from src.to_neo4j import dataToNeo4j
import json


class zhongGuoYiYao():
    def __init__(self):
        self.save_path = "../file/中国医药信息查询平台/"
        self.Neo = dataToNeo4j()

    def disease_name(self):
        with open(self.save_path + 'disease_name.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                for element in temp:
                    if self.Neo.query_node("疾病名称", name=element) == 0:
                        self.Neo.create_node("疾病名称", element)
                disease = temp[0]
                print(temp)
                for i in range(1, len(temp)):
                    status = self.Neo.relat_exists("疾病名称", disease, temp[i], '疾病别名')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病名称", [[disease, temp[i]]], '疾病别名')

    def symptom(self):
        with open(self.save_path + 'symptom.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                temp_split = re.split("[、，,]", temp[1])
                for element in temp_split:
                    print(element)
                    if self.Neo.query_node("疾病症状", name=element) == 0:
                        self.Neo.create_node("疾病症状", element)
                for i in range(len(temp_split)):
                    print(temp_split[i])
                    status = self.Neo.relat_exists("疾病名称", temp[0], temp_split[i], '疾病典型症状')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病症状", [[temp[0], temp_split[i]]], '疾病典型症状')

    def disease_name_en(self):
        with open(self.save_path + 'name.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("疾病英文名称", name=element) == 0:
                        self.Neo.create_node("疾病英文名称", element)
                    status = self.Neo.relat_exists("疾病名称", disease, element, '疾病英文名')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病英文名称", [[disease, element]], '疾病英文名')

    def disease_inspectionItem(self):
        with open(self.save_path + 'inspectionItem.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]

                for element in name:
                    print(element)
                    if self.Neo.query_node("疾病检查", name=element) == 0:
                        self.Neo.create_node("疾病检查", element)
                    status = self.Neo.relat_exists("疾病名称", disease, element, '疾病对应检查')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病检查", [[disease, element]], '疾病对应检查')

    def disease_relatedDrug(self):
        with open(self.save_path + 'relatedDrug.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]

                for element in name:
                    print(element)
                    if self.Neo.query_node("疾病药品", name=element) == 0:
                        self.Neo.create_node("疾病药品", element)
                    status = self.Neo.relat_exists("疾病名称", disease, element, '疾病对应药品')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病药品", [[disease, element]], '疾病对应药品')

    def disease_parts(self):
        with open(self.save_path + 'parts.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    print(element)
                    if self.Neo.query_node("疾病部位", name=element) == 0:
                        self.Neo.create_node("疾病部位", element)
                    status = self.Neo.relat_exists("疾病名称", disease, element, '疾病对应部位')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病部位", [[disease, element]], '疾病对应部位')

    def disease_mainCauses(self):
        with open(self.save_path + 'mainCauses.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = [i for i in re.split("[、，,]", ''.join(temp[1:])) if i != '']
                for element in name:
                    print(element)
                    if self.Neo.query_node("疾病主要因素", name=element) == 0:
                        self.Neo.create_node("疾病主要因素", element)
                    status = self.Neo.relat_exists("疾病名称", disease, element, '疾病对应主要因素')
                    if status:
                        self.Neo.create_relation_ship('疾病名称', "疾病主要因素", [[disease, element]], '疾病对应主要因素')

    def disease_multiplePopulation(self):
        with open(self.save_path + 'multiplePopulation.txt', encoding="utf8")as f:
            for line in f:
                temp_ = [i for i in line.replace("\n", '').split("\t") if i != '']
                temp = [i for i in re.split("<p>(.*?)</p>", ''.join(temp_[1:])) if i != '']
                if len(temp)>1:
                    print(temp, temp_[0])

    def syptom_name(self):
        with open(self.save_path + 'syptom.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in line.replace("\n", '').split("\t") if i != '']
                disease = temp[0]
                name = temp[1:]
                if self.Neo.query_node("疾病症状", name=disease) == 0:
                    self.Neo.create_node("疾病症状", disease)
                for element in name:
                    if self.Neo.query_node("疾病症状", name=element) == 0:
                        self.Neo.create_node("疾病症状", element)
                    status = self.Neo.relat_exists("疾病症状", disease, element, '疾病症状别名')
                    if status:
                        print(disease, element)
                        self.Neo.create_relation_ship('疾病症状', "疾病症状", [[disease, element]], '疾病症状别名')
    def syptom_dis(self):
        with open(self.save_path + 'syptom_dis.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in re.split("[\t; ]",line.replace("\n", '')) if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    try:
                        if self.Neo.query_node("疾病名称", name=element) == 0:
                            self.Neo.create_node("疾病名称", element)
                            print(element)
                        status = self.Neo.relat_exists("疾病症状", disease, element, '症状相关疾病')
                        if status:
                            print(disease, element)
                            self.Neo.create_relation_ship('疾病症状', "疾病名称", [[disease, element]], '症状相关疾病')
                    except:
                        pass
    def syptom_cause(self):
        with open(self.save_path + 'syptom_dis_cause.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in re.split("[\t; ]",line.replace("\n", '')) if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    try:
                        if self.Neo.query_node("症状主要因素", name=element) == 0:
                            self.Neo.create_node("症状主要因素", element)
                            print(element)
                        status = self.Neo.relat_exists("疾病症状", disease, element, '症状相关主要因素')
                        if status:
                            print(disease, element)
                            self.Neo.create_relation_ship('疾病症状', "症状主要因素", [[disease, element]], '症状相关主要因素')
                    except:
                        print(disease, element,"fail")
                        pass
    def syptom_inspectionItems(self):
        with open(self.save_path + 'syptom_inspectionItems.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in re.split("[\t; ]",line.replace("\n", '')) if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    try:
                        if self.Neo.query_node("疾病检查", name=element) == 0:
                            self.Neo.create_node("疾病检查", element)
                            print(element)
                        status = self.Neo.relat_exists("疾病症状", disease, element, '症状对应检查')
                        if status:
                            print(disease, element)
                            self.Neo.create_relation_ship('疾病症状', "疾病检查", [[disease, element]], '症状对应检查')
                    except:
                        print(disease, element,"fail")
                        pass
    def syptom_parts(self):
        with open(self.save_path + 'syptom_parts.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in re.split("[\t; ]",line.replace("\n", '')) if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    try:
                        if self.Neo.query_node("疾病部位", name=element) == 0:
                            self.Neo.create_node("疾病部位", element)
                            print(element)
                        status = self.Neo.relat_exists("疾病症状", disease, element, '症状对应部位')
                        if status:
                            print(disease, element)
                            self.Neo.create_relation_ship('疾病症状', "疾病部位", [[disease, element]], '症状对应部位')
                    except:
                        print(disease, element,"fail")
                        pass
    def syptom_drugTherapy(self):
        with open(self.save_path + 'syptom_drugTherapy.txt', encoding="utf8")as f:
            for line in f:
                temp = [i for i in re.split("[\t; ]",line.replace("\n", '')) if i != '']
                disease = temp[0]
                name = temp[1:]
                for element in name:
                    try:
                        if self.Neo.query_node("疾病药品", name=element) == 0:
                            self.Neo.create_node("疾病药品", element)
                            print(element)
                        status = self.Neo.relat_exists("疾病症状", disease, element, '症状对应药品')
                        if status:
                            print(disease, element)
                            self.Neo.create_relation_ship('疾病症状', "疾病药品", [[disease, element]], '症状对应药品')
                    except:
                        print(disease, element,"fail")
                        pass



zhongGuoYiYao().syptom_drugTherapy()
